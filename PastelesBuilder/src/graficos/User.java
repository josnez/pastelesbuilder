package graficos;

import java.util.Scanner;

import pasteles.*;

public class User {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner(System.in);
		int pisos, balcones, habitaciones, ventanas, puertas, residentes;
		
		System.out.println("Digite la cantidad; de pisos, balcones, habitaciones, ventanas, puertas, residentes: ");
		
		pisos = lector.nextInt();
		balcones = lector.nextInt();
		habitaciones = lector.nextInt();
		ventanas = lector.nextInt();
		puertas = lector.nextInt();
		residentes = lector.nextInt();
		
		Edificio edificio = new PreparacionBuilder().pisos(pisos).balcones(balcones).habitaciones(habitaciones).
				ventanas(ventanas).puertas(puertas).residentes(residentes).build();
		System.out.println(edificio.toString());
	}

}
