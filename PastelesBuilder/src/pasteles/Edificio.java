package pasteles;

public class Edificio {
	
	private int habitaciones;
	private int ventanas;
	private int puertas;
	private int balcones;
	private int pisos;
	private int residentes;
	
	public void setHabitaciones(int habitaciones) {
		this.habitaciones = habitaciones;
	}
	public void setVentanas(int ventanas) {
		this.ventanas = ventanas;
	}
	public void setPuertas(int puertas) {
		this.puertas = puertas;
	}
	public void setBalcones(int balcones) {
		this.balcones = balcones;
	}
	public void setPisos(int pisos) {
		this.pisos = pisos;
	}
	public void setResidentes(int residentes) {
		this.residentes = residentes;
	}
	
	public String toString() {
		return "Edificio con: \nPisos: "+pisos+"\nBalcones: "+balcones+"\nHabitaciones: "+habitaciones+"\nVentanas: "
				+ventanas+"\nPuertas: "+puertas+"\nResidentes: "+residentes;
	}
}
