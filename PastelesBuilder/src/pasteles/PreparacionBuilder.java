package pasteles;

public class PreparacionBuilder {
	
	private Edificio edificio = new Edificio();
	
	public PreparacionBuilder() {
		
	}
	
	public PreparacionBuilder pisos(int pisos) {
		
		this.edificio.setPisos(pisos);
		return this;
	}
	
	public PreparacionBuilder balcones(int balcones) {
		
		this.edificio.setBalcones(balcones);
		return this;
	}
	
	public PreparacionBuilder habitaciones(int habitaciones) {
		
		this.edificio.setHabitaciones(habitaciones);
		return this;
	}
	
	public PreparacionBuilder ventanas(int ventanas) {
		
		this.edificio.setVentanas(ventanas);
		return this;
	}
	
	public PreparacionBuilder puertas(int puertas) {
		this.edificio.setPuertas(puertas);
		return this;
	}
	
	public PreparacionBuilder residentes(int residentes) {
		
		this.edificio.setResidentes(residentes);
		return this;
	}
	
	public Edificio build() {
		return edificio;
	}
}
